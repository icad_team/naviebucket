#include "Coordinates.h"

using namespace Coordinates;


Point Point::operator+(Point point_r) {
	Point result;
	result.x = x + point_r.x;
	result.y = y + point_r.y;
	result.z = z + point_r.z;
	return result;
}

Point Point::operator-(Point point_r) {
	Point result;
	result.x = x - point_r.x;
	result.y = y - point_r.y;
	result.z = z - point_r.z;
	return result;
}

Point Point::operator-() {
	Point result;
	result.x = -x;
	result.y = -y;
	result.z = -z;
	return result;
}

Point Point::operator=(Point point_r) {
	Point result;
	result.x = x = point_r.x;
	result.y = y = point_r.y;
	result.z = z = point_r.z;
	return result;
}

Point Point::operator*(double number_r) {
	Point result;
	result.x = number_r * x;
	result.y = number_r * y;
	result.z = number_r * z;
	return result;
}

double Point::norm() {
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}


//void Point::SetPoint(double in_x, double in_y, double in_z) {
//	this->x = in_x;
//	this->y = in_y;
//	this->z = in_z;
//}

Point::Point(double in_x, double in_y, double in_z) {
	this->x = in_x;
	this->y = in_y;
	this->z = in_z;
}
Point::Point(std::ifstream &in_stream) {
	in_stream >> this->x >> this->y >> this->z;
}

Point::Point() {
}

double Point::GetX() {
	return this->x;
}

double Point::GetY() {
	return this->y;
}

double Point::GetZ() {
	return this->z;
}


void Point::WriteInFile(std::ofstream& out_file) {
	//out_file << this->x << "\t" << this->y << "\t" << this->z << "\t" << this->t << endl;
	out_file << this->x << "\t" << this->y << "\t" << this->z << endl;
}

double Vector::norm() {
	return this->ToPoint().norm();
}

Point Vector::ToPoint() {
	Point result(this->GetX(), this->GetY(), this->GetZ());
	return result;
}

double Vector::operator,(Vector point_r) {
	return x * point_r.x + y * point_r.y + z * point_r.z;
}

double Vector::operator,(Point point_r) {
	return this->x * point_r.GetX() + this->y * point_r.GetY() + this->z * point_r.GetZ();
}

Vector Vector::operator*(Vector point_r) {
	Vector vec_result;
	vec_result.x = point_r.z * this->y - point_r.y * this->z;
	vec_result.y = -point_r.z * this->x + this->z * point_r.x;
	vec_result.z = point_r.y * this->x - point_r.x * this->y;
	return vec_result;
}

Vector Vector::operator*(double number_r) {
	Vector result;
	result.x = number_r * x;
	result.y = number_r * y;
	result.z = number_r * z;
	return result;
}

Vector Vector::operator+(Vector vector_r) {
	Vector result;
	result.x = this->x + vector_r.x;
	result.y = this->y + vector_r.y;
	result.z = this->z + vector_r.z;
	return result;
}

Vector Vector::operator-(Vector vector_r) {
	Vector result;
	result.x = this->x - vector_r.x;
	result.y = this->y - vector_r.y;
	result.z = this->z - vector_r.z;
	return result;
}

Vector::Vector(Point point_from, Point point_to) {
	Point vec = point_to - point_from;
	this->x = vec.GetX();
	this->y = vec.GetY();
	this->z = vec.GetZ();
}

Vector::Vector() {
}

Vector::Vector(double in_x, double in_y, double in_z) {
	this->x = in_x;
	this->y = in_y;
	this->z = in_z;
}
