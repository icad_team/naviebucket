#include "CoordinatesN.h"

using namespace PointsN;
		
template<typename T, int nDimensions>
T const& PointN<T, nDimensions>::operator[](int const i) const {
	return elements_[i];
}

template<typename T, int nDimensions>
void PointN<T, nDimensions>::operator+=(PointN<T, nDimensions> const& other) {
	for (int i = 0; i < nDimensions; ++i) {
		elements_[i] += other.elements_[i];
	}
}

template<typename T, int nDimensions>
void PointN<T, nDimensions>::operator-=(PointN<T, nDimensions> const& other) {
	for (int i = 0; i < nDimensions; ++i) {
		elements_[i] -= other.elements_[i];
	}
}

template<typename T, int nDimensions>
T& PointN<T, nDimensions>::operator=(PointN<T, nDimensions> const& other) {
	this->elements_ = other->elements_;
	return this;
}

template<typename T, int nDimensions>
PointN<T, nDimensions> PointN<T, nDimensions>::operator+(PointN<T, nDimensions> const& a) {
	PointN<T, nDimensions> result = this;
	result += a;
	return result;
}

template<typename T, int nDimensions>
PointN<T, nDimensions> PointN<T, nDimensions>::operator-(PointN<T, nDimensions> const&a) {
	PointN<T, nDimensions> result = this;
	result -= a;
	return result;
}

template<typename T, int nDimensions>
PointN<T, nDimensions>::PointN(std::ifstream &in_stream) {
	for (int i = 0; i < nDimensions; ++i) {
		in_stream >> elements_[i];
	}
}

/*template<typename T, int nDimensions>
VectorN<T, nDimensions> PointN<T, nDimensions>::ToVector() {
	VectorN<T, nDimensions> result(this);
	return result;
}*/

template<typename T, int nDimensions>
PointN<T, nDimensions>::PointN() : elements_() {}

template<typename T, int nDimensions>
PointN<T, nDimensions>::PointN(T x, T y)
{
	STATIC_ASSERT(nDimensions == 2);
	elements_[0] = x;
	elements_[1] = y;
}

template<typename T, int nDimensions>
PointN<T, nDimensions>::PointN(T x, T y, T z)
{
	STATIC_ASSERT(nDimensions == 3);
	elements_[0] = x;
	elements_[1] = y;
	elements_[2] = z;
}

// Vector functions

template<typename T, int nDimensions>
T VectorN<T, nDimensions>::operator,(VectorN<T, nDimensions> const& other) {
	T result = this->elements_[0] * other.elements_[0];
	for (int i = 1; i < nDimensions; ++i) {
		result += this->elements_[i] * other.elements_[i];
	}
	return result;
}
	
template<typename T, int nDimensions>
VectorN<T, nDimensions> VectorN<T, nDimensions>::CalculateSymmetry(int direction) {
	T result = this;
	result[direction] = -result[direction];
		
}


template<typename T, int nDimensions>
VectorN<T, nDimensions>::VectorN() : elements_() {}

template<typename T, int nDimensions>
VectorN<T, nDimensions>::VectorN(T x, T y)
{
	STATIC_ASSERT(nDimensions == 2);
	elements_[0] = x;
	elements_[1] = y;
}

template<typename T, int nDimensions>
VectorN<T, nDimensions>::VectorN(T x, T y, T z)
{
	STATIC_ASSERT(nDimensions == 3);
	elements_[0] = x;
	elements_[1] = y;
	elements_[2] = z;
}


