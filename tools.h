#include <locale>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <Eigen/Dense>
#include <Eigen/LU>
#include <vector>

using Eigen::Matrix3d;
using Eigen::MatrixXd;

namespace tools {
	std::vector<int> extract_ints(std::ctype_base::mask category, std::string str, std::ctype<char> const& facet);

	std::vector<int> extract_ints(std::string str);
}

namespace EigenExtensions {
	double CalculateMinor(MatrixXd matrix, int i, int j);
	void removeColumn(Eigen::MatrixXd& matrix, unsigned int colToRemove);
	void removeRow(Eigen::MatrixXd& matrix, unsigned int rowToRemove);
}