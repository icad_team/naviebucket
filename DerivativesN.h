#ifndef _DERIVATIVESN_H_
#define _DERIVATIVESN_H_

#include "GridsN.h"

using std::vector;
using namespace GridsN;

namespace DerivativesN {
	enum DerivativeType { 
		First, // Derivative - central difference
		FirstPlus, // Derivative - right difference
		FirstMinus, // Derivative - left difference
		AvgPlus, // Average value - right and this
		AvgMinus, // Average value - right and this
		Second, // Second derivative
		SmoothSecond // Navie smooth formula
	};

	// calculate derivative in special direction
	template<class TPoint>
	TPoint static CalculateDerivative(Index position, int direction, const GridsN::GridN<TPoint>& points, DerivativeType derivative_type, const vector<double>& h_steps) {
		Index direction_sizes = points.GetDirectionSizes();
		Index plus_point = position;
		plus_point[direction] += 1;
		Index minus_point = position;
		minus_point[direction] -= 1;

		switch (derivative_type) {
		case FirstPlus:
			if (position[direction] == direction_sizes[direction] - 1) {
				return CalculateDerivative(position, direction, points, FirstMinus, h_steps);
			}
			return (points[plus_point] - points[position]) / h_steps[direction];
		case FirstMinus:
			if (position[direction] == 0) {
				return CalculateDerivative(position, direction, points, FirstPlus, h_steps);
			}
			return (points[position] - points[minus_point]) / h_steps[direction];
		case AvgPlus:
			if (position[direction] == direction_sizes[direction] - 1) {
				points[position];
			}
			return (points[plus_point] + points[position]) / 2.;
		case AvgMinus:
			if (position[direction] == 0) {
				points[position];
			}
			return (points[position] + points[minus_point]) / 2.;
		case First:
			if (position[direction] == 0) {
				return CalculateDerivative(position, direction, points, FirstPlus, h_steps);
			}
			else if (position[direction] == direction_sizes[direction] - 1) {
				return CalculateDerivative(position, direction, points, FirstMinus, h_steps);
			}
			else {
				return (points[plus_point] - points[minus_point]) * 0.5 / h_steps[direction];
			}
		case Second:
			if (position[direction] == 0) {
				return CalculateDerivative(plus_point, direction, points, derivative_type, h_steps);
			}
			else if (position[direction] == direction_sizes[direction] - 1) {
				return CalculateDerivative(minus_point, direction, points, derivative_type, h_steps);
			}
			return points[plus_point] - points[position] * 2. + points[minus_point];
		case SmoothSecond:
			if (position[direction] == 0) {
				return CalculateDerivative(plus_point, direction, points, derivative_type, h_steps);
			}
			else if (position[direction] == direction_sizes[direction] - 1) {
				return CalculateDerivative(minus_point, direction, points, derivative_type, h_steps);
			}
			return points[plus_point] + points[position] * 2. + points[minus_point];
		}
	}

	// Calculate derivatives in all directions
	template<class TPoint>
	class DerivativeFirstN {
	public:

		DerivativeFirstN(const GridsN::GridN<TPoint>& points, DerivativeType derivative_type, vector<double> h_steps = vector<double>()) {
			this->direction_sizes_ = points.GetDirectionSizes();
		
			this->h_steps_ = h_steps;
			if (h_steps.empty()) {
				for (int i = 0; i < this->direction_sizes_.size(); ++i) {
					this->h_steps_.push_back(1. / (this->direction_sizes_[i] - 1.));
				}
			}

			int size = this->CalculateSize();
			this->derivatives_ = new TPoint[size];

			int count_points = size / direction_sizes_.size();

			// Calculate derivatives in each direction
			for (int global_index = 0; global_index < size; ++global_index) {
				int global_position = global_index;
				Index position;
				for (int dimension_number = 0; dimension_number < direction_sizes_.size(); ++dimension_number) {
					position.push_back(global_position % direction_sizes_[dimension_number]);
					global_position /= direction_sizes_[dimension_number];
				}
				int direction = global_position;
				this->derivatives_[global_index] = CalculateDerivative(position, direction, points, derivative_type, h_steps_);
				//Point3D res(CalculateDerivative(position, direction, points, derivative_type));
				//std::cout << position[0] << "\t" << position[1] << "\t glob:" << global_index << "\t dir: " << direction << "\t res:" << res[0] << "\t" << res[1] << "\t" << res[2] << std::endl;
			}
		}

		DerivativeFirstN(const vector<int> direction_sizes) {
			this->direction_sizes_ = direction_sizes;

			int size = this->CalculateSize();
			this->derivatives_ = new TPoint[size];
		}

		~DerivativeFirstN() {
			delete[] derivatives_;
		}

		TPoint& operator()(Index position, int direction) {
			//std::cout << "global" << "\t" << this->CalculateGlobalPosition(position, direction) << std::endl;
			return *(this->derivatives_ + this->CalculateGlobalPosition(position, direction));
		}

		TPoint GetPosition(Index position, int direction) {
			TPoint result = *(this->derivatives_ + this->CalculateGlobalPosition(position, direction));
			return result;
		}		

		std::shared_ptr<GridN<TPoint> > GetDirection(int direction) {
			std::shared_ptr<GridN<TPoint> > result(new GridN<TPoint>(this->direction_sizes_));
			

			for (int global_index = 0; global_index < (*result).GetSize(); ++global_index) {
				int global_position = global_index;
				Index position;
				for (int dimension_number = 0; dimension_number < direction_sizes_.size(); ++dimension_number) {
					position.push_back(global_position % direction_sizes_[dimension_number]);
					global_position /= direction_sizes_[dimension_number];
				}
				(*result)[position] = this->operator()(position, direction);
			}
			return result;
		}
	private:
		TPoint* derivatives_;
		vector<int> direction_sizes_;
		vector<double> h_steps_;

		// size = (count of directions) * (number of points)
		int CalculateSize() {
			int size = direction_sizes_.size(); // count of directions
			// number of points
			for (int dimension_number = 0; dimension_number < direction_sizes_.size(); ++dimension_number) {
				size *= direction_sizes_[dimension_number];
			}
			return size;
		}

		int CalculateGlobalPosition(Index position, int direction) {
			if (position.size() != this->direction_sizes_.size()) {
				throw;
			}
			int global_position = direction;
			for (int dimension = position.size() - 1; dimension >= 0; --dimension) {
				global_position = position[dimension] + this->direction_sizes_[dimension] * global_position;
			}
			return global_position;
		}			
	};

	template<class T>
	std::shared_ptr<GridN<T> > CalculateDerivativeDirection(const GridN<T>& grid, int direction, DerivativeType derivative_type, vector<double> h_steps) {

		std::shared_ptr<GridN<T> > result(new GridN<T>(grid.GetDirectionSizes()));
		//result = std::make_shared<GridN<T> >(GridN<T>(grid.GetDirectionSizes()));
		Index direction_sizes_ = grid.GetDirectionSizes();
		// Calculate derivatives in each direction
		for (int global_index = 0; global_index < grid.GetSize(); ++global_index) {
			int global_position = global_index;
			Index position;
			for (int dimension_number = 0; dimension_number < direction_sizes_.size(); ++dimension_number) {
				position.push_back(global_position % direction_sizes_[dimension_number]);
				global_position /= direction_sizes_[dimension_number];
			}
			(*result)[position] = CalculateDerivative(position, direction, grid, derivative_type, h_steps);
		}
		return result;
	}
}
#endif