#include "GridsN.h"

using namespace GridsN;

int GetPositionByIndex(std::vector<int> const index, std::vector<int> const direction_sizes) {
	if (index.size() != direction_sizes.size()) {
		throw;
	}
	int position = 0;
	for (int direction = 0; direction < index.size(); ++direction) {
		position = index[direction] + direction_sizes[direction] * position;
	}
}

template <class T>
T& GridN<T>::operator[](Index position) {
	return this->elements_ + GetPositionByIndex(position, _this->direction_sizes);
}

template <class T>
GridN<T>::GridN<T>(const Index direction_sizes) {
	this->_direction_sizes = direction_sizes;


	int size = 1;
	for (int dimension_number = 0; dimension_number < direction_sizes_.size(); ++dimension_number) {
		size *= _direction_sizes[dimension_number];
	}

	this->_elements = new TPoint[size];
}

template <class T>
GridN<T>::~GridN<T>() {
	delete[] elements_;
}

