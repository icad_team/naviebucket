#include "Navie.h"

using namespace Navie;
using namespace GridsN;
using namespace DerivativesN;

Mesh::Mesh(string file_name) {
	std::ifstream in_stream(file_name);

	// Read head
	string line;
	std::getline(in_stream, line); //VARIABLES = "X", "Y", "Z"
	std::getline(in_stream, line); //ZONE F=POINT, I=16, J=17, K=6

	vector<int> sizes = tools::extract_ints(line);
	this->I = sizes[0] + 4;
	this->K = sizes[2];
	this->J = sizes[1];

	// Calculate steps
	this->h_step = { 1. / (this->I - 5), 1. / (this->J - 1), 1. / (this->K - 1) };
	Index SIZES = { I, J, K };
	std::shared_ptr<GridN<Point3D> > coordinates(new GridN<Point3D>(SIZES));
	_coordinates = coordinates;
	std::shared_ptr<GridN<Point3D> > velocity(new GridN<Point3D>(SIZES));
	_velocity = velocity;
	std::shared_ptr<GridN<double> > density(new GridN<double>(SIZES));
	_density = density;
	std::shared_ptr<GridN<double> > preasure(new GridN<double>(SIZES));
	_preasure = preasure;
	std::shared_ptr<GridN<double> > energy(new GridN<double>(SIZES));
	_energy = energy;
	std::shared_ptr<GridN<double> > timestep(new GridN<double>(SIZES));
	_timestep = timestep;
	//std::shared_ptr<GridN<double> > temperature(new GridN<double>(SIZES));
	//_temperature = temperature;

	std::shared_ptr<GridN<double> > jacobian(new GridN<double>(SIZES));
	_jacobian = jacobian;
	// Read points
	for (int k = 0; k < this->K; ++k) {
		for (int j = 0; j < this->J; ++j) {
			for (int i = 2; i < this->I - 2; ++i) {
				Point3D coordinates(in_stream);
				Index current_index = { i, j, k };
				(*this->_coordinates)[current_index] = coordinates;
			}
			Index ind_this, ind_sym;
			ind_this = { 0, j, k };
			ind_sym = { 4, j, k };
			(*this->_coordinates)[ind_this] = (*this->_coordinates)[ind_sym].CalculateSymmetry(0);
			ind_this = { 1, j, k };
			ind_sym = { 3, j, k };
			(*this->_coordinates)[ind_this] = (*this->_coordinates)[ind_sym].CalculateSymmetry(0);
			ind_this = { I - 1, j, k };
			ind_sym = { I - 5, j, k };
			(*this->_coordinates)[ind_this] = (*this->_coordinates)[ind_sym].CalculateSymmetry(0);
			ind_this = { I - 2, j, k };
			ind_sym = { I - 4, j, k };
			(*this->_coordinates)[ind_this] = (*this->_coordinates)[ind_sym].CalculateSymmetry(0);
		}
	}

	in_stream.close();
	std::shared_ptr<DerivativeFirstN<Point3D> > first_derivatives(new DerivativeFirstN<Point3D>(*_coordinates, First, this->h_step));
	_first_derivatives = first_derivatives;

	// �������
	for (int k = 0; k < this->K; ++k) {
		for (int i = 0; i < this->I; ++i) {
			Index current_index = { i, 0, k };
			Index next_index = { i, 1, k };
			(*_first_derivatives)(current_index, 0) = (*_first_derivatives)(next_index, 0); // ksi_x
		}
	}


	std::shared_ptr<DerivativeFirstN<Point3D> > first_derivatives_inv(new DerivativeFirstN<Point3D>(_coordinates->GetDirectionSizes()));
	_first_derivatives_inv = first_derivatives_inv;
	
	// Jacobian
	for (int i = 0; i < I; ++i) {
		for (int j = 0; j < J; ++j) {
			for (int k = 0; k < K; ++k) {
				Index current_index = { i, j, k };
				if (current_index == vector<int>{0, 1, 3}) {
					int a = 0;
				}
				Matrix3d jacobi;
				jacobi(0, 0) = (*_first_derivatives)(current_index, 0)[0]; // ksi_x
				jacobi(0, 1) = (*_first_derivatives)(current_index, 0)[1]; // ksi_y
				jacobi(0, 2) = (*_first_derivatives)(current_index, 0)[2]; // ksi_z

				jacobi(1, 0) = (*_first_derivatives)(current_index, 1)[0]; // eta_x
				jacobi(1, 1) = (*_first_derivatives)(current_index, 1)[1]; // eta_y
				jacobi(1, 2) = (*_first_derivatives)(current_index, 1)[2]; // eta_z

				jacobi(2, 0) = (*_first_derivatives)(current_index, 2)[0]; // dzt_x
				jacobi(2, 1) = (*_first_derivatives)(current_index, 2)[1]; // dzt_y
				jacobi(2, 2) = (*_first_derivatives)(current_index, 2)[2]; // dzt_z
	
				Matrix3d jacobi_inv;
				for (int m = 0; m < 3; ++m) {
					for (int n = 0; n < 3; ++n) {
						jacobi_inv(m, n) = EigenExtensions::CalculateMinor(jacobi, m, n);
					}
				}

				(*_first_derivatives_inv)(current_index, 0) = Point3D(jacobi_inv(0, 0), jacobi_inv(0, 1), jacobi_inv(0, 2)) / jacobi.determinant();
				(*_first_derivatives_inv)(current_index, 1) = Point3D(jacobi_inv(1, 0), jacobi_inv(1, 1), jacobi_inv(1, 2)) / jacobi.determinant();
				(*_first_derivatives_inv)(current_index, 2) = Point3D(jacobi_inv(2, 0), jacobi_inv(2, 1), jacobi_inv(2, 2)) / jacobi.determinant();

				(*this->_jacobian)[current_index] = 1. / jacobi.determinant();
			}
		}
	}
}

Mesh::~Mesh() {
}

void Mesh::ReadParameters(std::ifstream& parameters_stream) {
	string line;
	std::getline(parameters_stream, line); //skip description
	std::getline(parameters_stream, line);
	MACH = std::stod(line);
	std::getline(parameters_stream, line); //skip description
	std::getline(parameters_stream, line);
	ATTACK_ANGLE = std::stod(line);
	ATTACK_ANGLE *= M_PI / 180.;
	std::getline(parameters_stream, line); //skip description
	std::getline(parameters_stream, line);
	RE_inv = std::stod(line);
	std::getline(parameters_stream, line); //skip description
	std::getline(parameters_stream, line);
	PRANDL = std::stod(line);
	std::getline(parameters_stream, line); //skip description
	std::getline(parameters_stream, line);
	CURRANT = std::stod(line);
	std::getline(parameters_stream, line); //skip description
	std::getline(parameters_stream, line);
	GAMMA = std::stod(line);
	std::getline(parameters_stream, line); //skip description
	std::getline(parameters_stream, line);
	TEMPERATURE_BOUNDARY = std::stod(line);
}

void Mesh::InitExternalProperties(double external_preasure, double external_density) {
	for (int i = 0; i < I; ++i) {
		for (int j = 0; j < J; ++j) {
			//int k = K - 1; // external boundary
			for (int k = 0; k < K; ++k) {
				Index current_index = { i, j, k };
				(*_density)[current_index] = external_density;
				(*_preasure)[current_index] = external_preasure;

				// velocity under angle of attack 
				Point3D velocity(0.0, MACH * sqrt(GAMMA) * sin(ATTACK_ANGLE), MACH * sqrt(GAMMA) * cos(ATTACK_ANGLE));
				(*_velocity)[current_index] = velocity;
				(*_energy)[current_index] = Mesh::CalculateTotalInternalEnergy(GAMMA, external_preasure, external_density, velocity);
			}
		}
	}
}
// As first 2 transverse points are symmitry
void Mesh::SetSymmetry() {
	for (int j = 0; j < J; ++j) {
		for (int k = 0; k < K; ++k) {

			Index index0 = { 0, j, k };
			Index index4 = { 4, j, k };
			(*_preasure)[index0] = (*_preasure)[index4];
			(*_density)[index0] = (*_density)[index4];
			//(*_temperature)[index0] = (*_temperature)[index4];
			(*_energy)[index0] = (*_energy)[index4];
			(*_velocity)[index0] = (*_velocity)[index4].CalculateSymmetry(0);

			Index index1 = { 1, j, k };
			Index index3 = { 3, j, k };
			(*_preasure)[index1] = (*_preasure)[index3];
			(*_density)[index1] = (*_density)[index3];
			//(*_temperature)[index1] = (*_temperature)[index3];
			(*_energy)[index1] = (*_energy)[index3];
			(*_velocity)[index1] = (*_velocity)[index3].CalculateSymmetry(0);

			Index indexI1 = { I - 1, j, k };
			Index indexI5 = { I - 5, j, k };
			(*_preasure)[indexI1] = (*_preasure)[indexI5];
			(*_density)[indexI1] = (*_density)[indexI5];
			//(*_temperature)[indexI1] = (*_temperature)[indexI5];
			(*_energy)[indexI1] = (*_energy)[indexI5];
			(*_velocity)[indexI1] = (*_velocity)[indexI5].CalculateSymmetry(0);

			Index indexI2 = { I - 2, j, k };
			Index indexI4 = { I - 4, j, k };
			(*_preasure)[indexI2] = (*_preasure)[indexI4];
			(*_density)[indexI2] = (*_density)[indexI4];
			//(*_temperature)[indexI2] = (*_temperature)[indexI4];
			(*_energy)[indexI2] = (*_energy)[indexI4];
			(*_velocity)[indexI2] = (*_velocity)[indexI4].CalculateSymmetry(0);
		}
	}
	for (int i = 0; i < I; ++i) {
		for (int j = 0; j < J; ++j) {
			// First slice dublicate second
			//Index index1 = { i, j, 0 };
			//Index index2 = { i, j, 1 };
			//(*_preasure)[index1] = (*_preasure)[index2];
			//(*_density)[index1] = (*_density)[index2];
			////(*_temperature)[index1] = (*_temperature)[index2];
			//(*_energy)[index1] = (*_energy)[index2];
			//(*_velocity)[index1] = (*_velocity)[index2];

			// Last 2 slices dublicate
			Index indexK1 = { i, j, K - 1 };
			Index indexK2 = { i, j, K - 2 };
			Index indexK3 = { i, j, K - 3 };
			(*_preasure)[indexK1] = (*_preasure)[indexK2] = (*_preasure)[indexK3];
			(*_density)[indexK1] = (*_density)[indexK2] = (*_density)[indexK3];
			//(*_temperature)[indexK1] = (*_temperature)[indexK2] = (*_temperature)[indexK3];
			(*_energy)[indexK1] = (*_energy)[indexK2] = (*_energy)[indexK3];
			(*_velocity)[indexK1] = (*_velocity)[indexK2] = (*_velocity)[indexK3];
		}
	}
}

void Mesh::SetInternalBoundary() {
	//sticking on internal boundary
	Point3D velocity(0., 0., 0.);

	for (int i = 0; i < I; ++i) {
		for (int k = 0; k < K; ++k) {
			Index current_index = { i, 0, k };
			Index next_index = { i, 1, k };
			(*_velocity)[current_index] = velocity;
			(*_preasure)[current_index] = Mesh::CalculatePreasureByParameters(this->GAMMA, (*_energy)[next_index], (*_density)[next_index], (*_velocity)[next_index]);;
			(*_density)[current_index] = (*_preasure)[current_index] / TEMPERATURE_BOUNDARY;
			(*_energy)[current_index] = (*_preasure)[current_index] / (GAMMA - 1.);
		}
	}
}

//double Mesh::CalculatePreasure() {
//	double max_delta = 0;
//	Index max_delta_index = vector<int>{0, 0, 0};
//	for (int i = 0; i < I; ++i) {
//		for (int j = 0; j < J; ++j) {
//			for (int k = 0; k < K; ++k) {
//				Index current_index = { i, j, k };
//				double old_preasure = (*_preasure)[current_index];
//				(*_preasure)[current_index] = Mesh::CalculatePreasureByParameters(this->GAMMA, (*_energy)[current_index], (*_density)[current_index], (*_velocity)[current_index]);
//
//				if (max_delta < abs((*_preasure)[current_index] - old_preasure)) {
//					max_delta = abs((*_preasure)[current_index] - old_preasure);
//					max_delta_index = current_index;
//				}
//			}
//		}
//	}
//	Index check_index = vector<int>{28, 0, 10};
//	auto v = (*_velocity)[check_index];
//	auto d = (*_density)[check_index];
//	auto e = (*_energy)[check_index];
//	std::cout << "Check preasure: " << (*_preasure)[check_index] << std::endl;
//	//std::cout << "Max delta preasure: " << max_delta << std::endl;
//	//std::cout << "	Max delta preasure index: " << max_delta_index[0] << "\t" << max_delta_index[1] << "\t" << max_delta_index[2] << std::endl;
//	return max_delta;
//}

void Mesh::GetStatistics() {
	std::cout << "	Min preasure: " << _preasure->GetMin() << std::endl;
	std::cout << "	Max preasure: " << _preasure->GetMax() << std::endl;
}

double Mesh::CalculateTimeStep() {
	_min_timestep = Mesh::MAX_MIN_TIME_STEP;
	for (int i = 0; i < I; ++i) {
		for (int j = 0; j < J; ++j) {
			for (int k = 0; k < K; ++k) {
				Index ind = { i, j, k };
				 
				double current_timestep = std::min({
					Mesh::CalculateTimeStepDirectionByParameters(this->GAMMA, this->CURRANT, _first_derivatives_inv->GetPosition(ind, 0),
					(*_velocity)[ind], (*_preasure)[ind], (*_density)[ind], h_step[0]),
					Mesh::CalculateTimeStepDirectionByParameters(this->GAMMA, this->CURRANT, _first_derivatives_inv->GetPosition(ind, 1),
					(*_velocity)[ind], (*_preasure)[ind], (*_density)[ind], h_step[1]),
					Mesh::CalculateTimeStepDirectionByParameters(this->GAMMA, this->CURRANT, _first_derivatives_inv->GetPosition(ind, 2),
					(*_velocity)[ind], (*_preasure)[ind], (*_density)[ind], h_step[2])
				});
				(*_timestep)[ind] = current_timestep;
				_min_timestep = std::min({
					_min_timestep,
					current_timestep
				});
			}
		}
	}

	std::cout << "	Min time step: " << _min_timestep << std::endl;
	return _min_timestep;
}

double Mesh::CalculateTimeStepDirectionByParameters(double gamma, double currant, Point3D derivative_direction, 
	Point3D velocity, double preasure, double density, double h_step) {
	return currant * h_step /(abs((derivative_direction, velocity)) +sqrt(gamma * preasure / density * (derivative_direction, derivative_direction)));
}

// source 1: page 85
double Mesh::CalculateTotalInternalEnergy(double gamma, double preasure, double density, Point3D velocity) {
	return 1. / (gamma - 1.) * preasure + 0.5 * density * (velocity, velocity);
}

// source 1: page 85
double Mesh::CalculatePreasureByParameters(double gamma, double energy, double density, Point3D velocity) {
	return (gamma - 1.) * (energy - 0.5 * density * (velocity, velocity ));
}

void Mesh::CalculateViscous() {
	//velocity derivatives
	SharedGridPoint3D _velocity_eta_plus = CalculateDerivativeDirection(*_velocity, 1, FirstPlus, h_step);

	SharedGridPoint3D _deriv_eta_jac_plus = CalculateDerivativeDirection(*(*(*_first_derivatives_inv).GetDirection(1) / *_jacobian), 1, AvgPlus, h_step);
	SharedGridPoint3D _deriv_eta_plus = CalculateDerivativeDirection(*(*_first_derivatives_inv).GetDirection(1), 1, AvgPlus, h_step);

	SharedGridPoint3D _deriv_ksi = (*_first_derivatives_inv).GetDirection(0);
	SharedGridPoint3D _deriv_eta = (*_first_derivatives_inv).GetDirection(1);
	SharedGridPoint3D _deriv_dzt = (*_first_derivatives_inv).GetDirection(2);

	// scalar multiplication
	SharedGridDouble _m2_jac_plus = *(_deriv_eta_jac_plus->operator,<double>(*_velocity_eta_plus)) / 3.;
	SharedGridDouble _m1_jac_plus = CalculateDerivativeDirection(*(*_deriv_eta->operator,<double>(*_deriv_eta) / *_jacobian), 1, AvgPlus, h_step);
	SharedGridDouble _deriv_eta_velocity_avg_plus = CalculateDerivativeDirection(*_deriv_eta->operator,<double>(*_velocity), 1, AvgPlus, h_step);
	SharedGridDouble _T_eta_plus = CalculateDerivativeDirection(*((*_preasure) / (*_density)), 1, FirstPlus, h_step);
	SharedGridDouble _velocity_sqr_eta_plus = CalculateDerivativeDirection(*_velocity->operator,<double>(*_velocity), 1, FirstPlus, h_step);
	
	Index SIZES = { I, J, K };
	
	std::shared_ptr<GridN<Point5D> > E(new GridN<Point5D>(SIZES));
	_E = E;
	std::shared_ptr<GridN<Point5D> > F(new GridN<Point5D>(SIZES));
	_F = F;
	std::shared_ptr<GridN<Point5D> > G(new GridN<Point5D>(SIZES));
	_G = G;
	std::shared_ptr<GridN<Point5D> > S(new GridN<Point5D>(SIZES));
	_S = S;
/*
	std::shared_ptr<GridN<Point5D> > S_old(new GridN<Point5D>(SIZES));
	_S_old = S_old;*/

	for (int i = 0; i < this->I; ++i) {
		for (int j = 0; j < this->J; ++j) {
			for (int k = 0; k < this->K; ++k) {
				Index current_index = { i, j, k };
				double& density = (*_density)[current_index];
				double& preasure = (*_preasure)[current_index];
				double& energy = (*_energy)[current_index];

				double& m1_jac_plus = (*_m1_jac_plus)[current_index];
				double& m2_jac_plus = (*_m2_jac_plus)[current_index];
				Point3D& deriv_eta_plus = (*_deriv_eta_plus)[current_index];
				double& deriv_eta_velocity_avg_plus = (*_deriv_eta_velocity_avg_plus)[current_index];
				double& T_eta_plus = (*_T_eta_plus)[current_index];;
				double& velocity_sqr_eta_plus = (*_velocity_sqr_eta_plus)[current_index];;

				Point3D& velocity = (*_velocity)[current_index];
				Point3D& velocity_eta_plus = (*_velocity_eta_plus)[current_index];

				Point3D& deriv_ksi = (*_deriv_ksi)[current_index];
				Point3D& deriv_eta = (*_deriv_eta)[current_index];
				Point3D& deriv_dzt = (*_deriv_dzt)[current_index];

				

				Point5D E_bef_transform = Point5D(density * velocity[0],
					density * pow(velocity[0], 2)  + preasure,
					density * velocity[0] * velocity[1],
					density * velocity[0] * velocity[2],
					(energy + preasure) * velocity[0]
					);

				Point5D F_bef_transform = Point5D(density * velocity[1],
					density * velocity[0] * velocity[1],
					density * pow(velocity[1], 2) + preasure,
					density * velocity[1] * velocity[2],
					(energy + preasure) * velocity[1]
					);

				Point5D G_bef_transform  = Point5D(density * velocity[2],
					density * velocity[0] * velocity[2],
					density * velocity[1] * velocity[2],
					density * pow(velocity[2], 2) + preasure,
					(energy + preasure) * velocity[2]
					);

				(*_S)[current_index] = Point5D(0,
					m1_jac_plus * velocity_eta_plus[0] - m2_jac_plus * deriv_eta_plus[0],
					m1_jac_plus * velocity_eta_plus[1] - m2_jac_plus * deriv_eta_plus[1],
					m1_jac_plus * velocity_eta_plus[2] - m2_jac_plus * deriv_eta_plus[2],
					// �������
					m1_jac_plus * (GAMMA / (GAMMA - 1.) / PRANDL * T_eta_plus + /*velocity[1] **/ velocity_sqr_eta_plus / 2.) + m2_jac_plus * deriv_eta_velocity_avg_plus
					) * RE_inv;

				//(*_S_old)[current_index] = Point5D(0,
				//	m1_jac_plus * velocity_eta_plus[0] + m2_jac_plus * deriv_eta_plus[0],
				//	m1_jac_plus * velocity_eta_plus[1] + m2_jac_plus * deriv_eta_plus[1],
				//	m1_jac_plus * velocity_eta_plus[2] + m2_jac_plus * deriv_eta_plus[2],
				//	// �������
				//	m1_jac_plus * (GAMMA / (GAMMA - 1.) / PRANDL * T_eta_plus + velocity_sqr_eta_plus / 2.) + m2_jac_plus * deriv_eta_velocity_avg_plus
				//	) * RE_inv;


				(*_E)[current_index] = E_bef_transform * deriv_ksi[0] + F_bef_transform * deriv_ksi[1] + G_bef_transform * deriv_ksi[2];
				(*_F)[current_index] = E_bef_transform * deriv_eta[0] + F_bef_transform * deriv_eta[1] + G_bef_transform * deriv_eta[2];
				(*_G)[current_index] = E_bef_transform * deriv_dzt[0] + F_bef_transform * deriv_dzt[1] + G_bef_transform * deriv_dzt[2];
			}
		}
	}
}

void Mesh::CalculateStep(SharedGridPoint5D& U, DerivativeType derivative_type) {

	auto E = CalculateDerivativeDirection(*(*_E / *_jacobian), 0, derivative_type, h_step);
	auto F = CalculateDerivativeDirection(*(*_F / *_jacobian), 1, derivative_type, h_step);
	auto G = CalculateDerivativeDirection(*(*_G / *_jacobian), 2, derivative_type, h_step);
	auto S = CalculateDerivativeDirection(*_S, 1, FirstMinus, h_step);

	/*auto S_old = CalculateDerivativeDirection(*_S_old, 1, FirstMinus, h_step);
	Index SIZES = { I, J, K };
	std::shared_ptr<GridN<Point5D> > U_old(new GridN<Point5D>(SIZES));
	double max_delta = 0;
	double max_val = 0;
	double max_val2 = 0;
	Index inddd;*/

	for (int i = 0; i < this->I; ++i) {
		for (int j = 0; j < this->J-1; ++j) {
			for (int k = 1; k < this->K; ++k) {
				Index current_index = { i, j, k };
				(*U)[current_index] = (*U)[current_index] - ((*E)[current_index] + (*F)[current_index] + (*G)[current_index] - (*S)[current_index])
					*(*_jacobian)[current_index] * _min_timestep;

				/*(*U_old)[current_index] = (*U)[current_index] - ((*E)[current_index] + (*F)[current_index] + (*G)[current_index] - (*S_old)[current_index])
					*(*_jacobian)[current_index] * _min_timestep;

				if (((*U_old)[current_index][4] - (*U)[current_index][4]) > max_delta) {
					max_delta = (*U_old)[current_index][4] - (*U)[current_index][4];
					max_val = (*U_old)[current_index][4];
					max_val2 = (*U)[current_index][4];
					inddd = current_index;
				}*/
			}
		}
	}
	/*std::cout << "CHECK: " << inddd[0] << " " << inddd[1] << " " << inddd[2] << " delta " << max_delta << " VAL " << max_val << " VAL2 " << max_val2 << std::endl;*/
}

void Mesh::ExtractMacroparameters(const SharedGridPoint5D& U) {
	for (int i = 0; i < this->I; ++i) {
		for (int j = 0; j < this->J; ++j) {
			for (int k = 0; k < this->K; ++k) {
				Index current_index = { i, j, k };
				double density = (*U)[current_index][0];
				Point3D velocity = Point3D((*U)[current_index][1] / density, (*U)[current_index][2] / density, (*U)[current_index][3] / density);
				(*_density)[current_index] = density;
				(*_velocity)[current_index] = velocity;
				(*_energy)[current_index] = (*U)[current_index][4];
				(*_preasure)[current_index] = Mesh::CalculatePreasureByParameters(this->GAMMA, (*_energy)[current_index], (*_density)[current_index], (*_velocity)[current_index]);
			}
		}
	}
	//Index current_index = { i, j, k };
	//std::cout << "PPP " << (*_preasure)[]
}

SharedGridPoint5D Mesh::ExtractU() {
	Index SIZES = { I, J, K };
	std::shared_ptr<GridN<Point5D> > U(new GridN<Point5D>(SIZES));
	for (int i = 0; i < this->I; ++i) {
		for (int j = 0; j < this->J; ++j) {
			for (int k = 0; k < this->K; ++k) {
				Index current_index = { i, j, k };

				double& density = (*_density)[current_index];
				double& preasure = (*_preasure)[current_index];
				double& energy = (*_energy)[current_index];
				
				Point3D& velocity = (*_velocity)[current_index];

				(*U)[current_index] = Point5D(density,
					density * velocity[0],
					density * velocity[1],
					density * velocity[2],
					energy
					);
			}
		}
	}
	return U;
}

void Mesh::Smooth(double alpha, SharedGridPoint5D& U, const SharedGridPoint5D& U_glad) {


	SharedGridDouble glad_0 = (*CalculateDerivativeDirection(*_preasure, 0, Second, h_step) / *CalculateDerivativeDirection(*_preasure, 0, SmoothSecond, h_step));
	SharedGridDouble glad_1 = (*CalculateDerivativeDirection(*_preasure, 1, Second, h_step) / *CalculateDerivativeDirection(*_preasure, 1, SmoothSecond, h_step));
	SharedGridDouble glad_2 = (*CalculateDerivativeDirection(*_preasure, 2, Second, h_step) / *CalculateDerivativeDirection(*_preasure, 2, SmoothSecond, h_step));
	std::shared_ptr<DerivativeFirstN<Point5D> > u_glad_smooth(new DerivativeFirstN<Point5D>(*U_glad, Second, this->h_step));

	for (int i = 1; i < this->I - 1; ++i) {
		for (int j = 1; j < this->J - 1; ++j) {
			for (int k = 1; k < this->K - 1; ++k) {
				Index current_index = { i, j, k };
				
				(*U)[current_index] = (*U)[current_index]
					+ ((*u_glad_smooth)(current_index, 0) * abs((*glad_0)[current_index])
					+ (*u_glad_smooth)(current_index, 1) * abs((*glad_1)[current_index])
					+ (*u_glad_smooth)(current_index, 2) * abs((*glad_2)[current_index])) * alpha;
			}
		}
	}

	/*Index current_index = vector<int>{29, 1, 10};
	Index current_index_p = vector<int>{29 + 1, 1, 10};
	Index current_index_m = vector<int>{29 - 1, 1, 10};
	auto p = (*_preasure)[current_index];
	auto pp = (*_preasure)[current_index_p];
	auto pm = (*_preasure)[current_index_m];

	auto g0 = (*glad_0)[current_index];
	auto g1 = (*glad_1)[current_index];
	auto g2 = (*glad_2)[current_index];
	auto gg0 = (*u_glad_smooth)(current_index, 0);
	auto gg1 = (*u_glad_smooth)(current_index, 1);
	auto gg2 = (*u_glad_smooth)(current_index, 2);
	int a = 0;*/
}

void Mesh::WriteResult(string file_name) {
	std::cout << "Writing result in file: " + file_name << std::endl;

	std::ofstream out_file(file_name);
	out_file << "VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\", \"R\"" << std::endl;
	out_file << "ZONE F=POINT, I=" << I - 4 << ", J=" << J << ", K=" << K << std::endl;

	for (int k = 0; k < K; ++k) {
		for (int j = 0; j < J; ++j) {
				for (int i = 2; i < I - 2; ++i) {
				Index current_index = { i, j, k };
				Point3D& current_point = (*_coordinates)[current_index];

				out_file << current_point[0] << "\t" 
					<< current_point[1] << "\t" 
					<< current_point[2] << "\t" 
					<< (*_velocity)[current_index][0] << "\t"
					<< (*_velocity)[current_index][1] << "\t"
					<< (*_velocity)[current_index][2] << "\t"
					<< (*_preasure)[current_index] << "\t"
					<< (*_density)[current_index] << "\t"
					<< std::endl;
			}
		}
	}
	out_file.close();
}

void Mesh::Sweep(SharedGridPoint5D& U) {
	auto _delta_E = CalculateDerivativeDirection(*_E, 0, First, h_step);
	//auto F = CalculateDerivativeDirection(*(*_F / *_jacobian), 1, First, h_step);
	auto _delta_G = CalculateDerivativeDirection(*_G, 2, First, h_step);

	//velocity derivatives
	SharedGridPoint3D _velocity_eta_plus = CalculateDerivativeDirection(*_velocity, 1, FirstPlus, h_step);

	SharedGridPoint3D _deriv_eta_plus = CalculateDerivativeDirection(*(*_first_derivatives_inv).GetDirection(1), 1, AvgPlus, h_step);

	SharedGridPoint3D _deriv_ksi = (*_first_derivatives_inv).GetDirection(0);
	SharedGridPoint3D _deriv_eta = (*_first_derivatives_inv).GetDirection(1);
	SharedGridPoint3D _deriv_dzt = (*_first_derivatives_inv).GetDirection(2);

	// scalar multiplication
	SharedGridDouble _m1_plus = CalculateDerivativeDirection(*_deriv_eta->operator,<double>(*_deriv_eta), 1, AvgPlus, h_step);
	SharedGridDouble _m2_plus = *(_deriv_eta_plus->operator,<double>(*_velocity_eta_plus)) / 3.;
	SharedGridDouble _deriv_eta_velocity_avg_plus = CalculateDerivativeDirection(*_deriv_eta->operator,<double>(*_velocity), 1, AvgPlus, h_step);
	//SharedGridDouble _T_eta_plus = CalculateDerivativeDirection(*((*_preasure) / (*_density)), 1, FirstPlus, h_step);
	//SharedGridDouble _velocity_sqr_eta_plus = CalculateDerivativeDirection(*_velocity->operator,<double>(*_velocity), 1, FirstPlus, h_step);
	//SharedGridDouble _velocity_sqr_density_eta_plus = CalculateDerivativeDirection(*(*(_velocity->operator,<double>(*_velocity)) / (*_density)), 1, FirstPlus, h_step);
	SharedGridDouble _velocity_sqr = _velocity->operator,<double>(*_velocity);

	//SharedGridDouble velocity_0 = _deriv_ksi->operator,<double>(*_velocity);
	SharedGridDouble _velocity_eta = _deriv_eta->operator,<double>(*_velocity);
	//SharedGridDouble velocity_2 = _deriv_dzt->operator,<double>(*_velocity);
	SharedGridDouble _m2_velocity_eta = (*_m2_plus) * (*_deriv_eta_velocity_avg_plus);
	auto _m2_velocity_eta_derivative_eta = CalculateDerivativeDirection(*_m2_velocity_eta, 1, FirstMinus, h_step);

	double tau = _min_timestep;
	for (int i = 0; i < this->I; ++i) {
		for (int k = 0; k < this->K; ++k) {
			std::vector<std::vector<int>> v(10, std::vector<int>(5));
			vector<vector<double> > c1(5, vector<double>(J));
			vector<vector<double> > c2(5, vector<double>(J));
			vector<vector<double> > c3(5, vector<double>(J));
			vector<vector<double> > d(5, vector<double>(J));

			for (int j = 1; j < this->J - 1; ++j) {
				Index ind = { i, j, k };
				Index ind_next = { i, j + 1, k };
				Index ind_prev = { i, j - 1, k };

				// scalar multiplication velocity and eta derivative
				double& V_eta = (*_velocity_eta)[ind];
				double& V_eta_next = (*_velocity_eta)[ind_next];
				double& V_eta_prev = (*_velocity_eta)[ind_prev];

				double& h2 = h_step[1];

				double& density_next = (*_density)[ind_next];
				double& density_prev = (*_density)[ind_prev];
				double& density = (*_density)[ind];

				double& m1_plus = (*_m1_plus)[ind];
				double& m1_mnus = (*_m1_plus)[ind_prev];

				double& m2_velocity_eta_derivative_eta = (*_m2_velocity_eta_derivative_eta)[ind];

				Point3D& eta_plus = (*_deriv_eta_plus)[ind];
				Point3D& eta_mnus = (*_deriv_eta_plus)[ind_prev];

				Point3D& eta_next = (*_deriv_eta)[ind];
				Point3D& eta_prev = (*_deriv_eta)[ind_prev];

				Point3D& velocity = (*_velocity)[ind];

				double& velocity_sqr = (*_velocity_sqr)[ind];
				double& velocity_sqr_next = (*_velocity_sqr)[ind_next];
				double& velocity_sqr_prev = (*_velocity_sqr)[ind_prev];

				Point3D& velocity_eta_plus = (*_velocity_eta_plus)[ind];
				Point3D& velocity_eta_mnus = (*_velocity_eta_plus)[ind_prev];

				double& preasure = (*_preasure)[ind];
				double& preasure_next = (*_preasure)[ind_next];
				double& preasure_prev = (*_preasure)[ind_prev];

				double& energy = (*_energy)[ind];

				Point5D& delta_E = (*_delta_E)[ind];
				Point5D& delta_G = (*_delta_G)[ind];

				// 2 coordinate
				c3[1][j] = tau / (2. * h2) * (density_next * V_eta_next) - tau * RE_inv * (m1_plus + pow(eta_plus[0], 2) / 3.) / pow(h2, 2);
				c1[1][j] = -tau / (2. * h2) * (density_prev * V_eta_prev) - tau * RE_inv * (m1_mnus + pow(eta_mnus[0], 2) / 3.) / pow(h2, 2);
				c2[1][j] = density + tau * RE_inv * (m1_plus + m1_mnus + pow(eta_plus[0], 2) / 3. + pow(eta_mnus[0], 2) / 3.) / pow(h2, 2);
				d[1][j] = density * velocity[0]
					- tau * (
					delta_E[1] +
					delta_G[1] +
					(preasure_next * eta_next[0] - preasure_prev * eta_prev[0]) / h2 / 2.)
					+ tau * RE_inv * (
					velocity_eta_plus[1] * eta_plus[0] * eta_plus[1] +
					velocity_eta_mnus[1] * eta_mnus[0] * eta_mnus[1] +
					velocity_eta_plus[2] * eta_plus[0] * eta_plus[2] +
					velocity_eta_mnus[2] * eta_mnus[0] * eta_mnus[2]) / h2;

				// 3 coordinate
				c3[2][j] = tau / (2. * h2) * (density_next * V_eta_next) - tau * RE_inv * (m1_plus + pow(eta_plus[1], 2) / 3.) / pow(h2, 2);
				c1[2][j] = -tau / (2. * h2) * (density_prev * V_eta_prev) - tau * RE_inv * (m1_mnus + pow(eta_mnus[1], 2) / 3.) / pow(h2, 2);
				c2[2][j] = density + tau * RE_inv * (m1_plus + m1_mnus + pow(eta_plus[1], 2) / 3. + pow(eta_mnus[1], 2) / 3.) / pow(h2, 2);
				d[2][j] = density * velocity[1]
					- tau * (
					delta_E[2] +
					delta_G[2] +
					(preasure_next * eta_next[1] - preasure_prev * eta_prev[1]) / h2 / 2.)
					+ tau * RE_inv * (
					velocity_eta_plus[0] * eta_plus[1] * eta_plus[0] +
					velocity_eta_mnus[0] * eta_mnus[1] * eta_mnus[0] +
					velocity_eta_plus[2] * eta_plus[1] * eta_plus[2] +
					velocity_eta_mnus[2] * eta_mnus[1] * eta_mnus[2]) / h2;

				// 4 coordinate
				c3[3][j] = tau / (2. * h2) * (density_next * V_eta_next) - tau * RE_inv * (m1_plus + pow(eta_plus[2], 2) / 3.) / pow(h2, 2);
				c1[3][j] = -tau / (2. * h2) * (density_prev * V_eta_prev) - tau * RE_inv * (m1_mnus + pow(eta_mnus[2], 2) / 3.) / pow(h2, 2);
				c2[3][j] = density + tau * RE_inv * (m1_plus + m1_mnus + pow(eta_plus[2], 2) / 3. + pow(eta_mnus[2], 2) / 3.) / pow(h2, 2);
				d[3][j] = density * velocity[2]
					- tau * (
					delta_E[3] +
					delta_G[3] +
					(preasure_next * eta_next[2] - preasure_prev * eta_prev[2]) / h2 / 2.)
					+ tau * RE_inv * (
					velocity_eta_plus[0] * eta_plus[2] * eta_plus[0] +
					velocity_eta_mnus[0] * eta_mnus[2] * eta_mnus[0] +
					velocity_eta_plus[1] * eta_plus[2] * eta_plus[1] +
					velocity_eta_mnus[1] * eta_mnus[2] * eta_mnus[1]) / h2;

				// 5 coordinate
				c3[4][j] = tau / (2 * h2) * GAMMA * V_eta_next - tau * RE_inv * m1_plus * GAMMA / PRANDL / density_next / pow(h2, 2);
				c1[4][j] = -tau / (2 * h2) * GAMMA * V_eta_prev - tau * RE_inv * m1_mnus * GAMMA / PRANDL / density_prev / pow(h2, 2);
				c2[4][j] = 1. + tau * RE_inv * GAMMA / PRANDL * (m1_plus / density_next + m1_mnus / density_prev) / pow(h2, 2);
				d[4][j] = energy - tau * (
					delta_E[4] +
					delta_G[4] +
					(GAMMA - 1.) * (V_eta_next * density_next * velocity_sqr_next / 2. - V_eta_prev * density_prev * velocity_sqr_prev / 2.) / h2 / 2.)
					+ tau *RE_inv * (
					m1_plus * ((-GAMMA / PRANDL + 1./*(velocity[1] + velocity_next[1]) / 2*/) * (velocity_sqr_next - velocity_sqr) / 2.)
					- m1_mnus * ((-GAMMA / PRANDL + 1./*(velocity[1] + velocity_prev[1]) / 2*/) * (velocity_sqr - velocity_sqr_prev) / 2.)) / pow(h2, 2)
					+ tau *RE_inv * m2_velocity_eta_derivative_eta;
			}
			vector<vector<double> > u_new(5, vector<double>(J));
			for (int component = 1; component < 5; ++component)	{
				vector<double> kk(J);
				vector<double> ll(J);
				kk[0] = 0;
				Index ind_0 = { i, 0, k };
				ll[0] = (*U)[ind_0][component];
				for (int j = 1; j < J - 1; ++j) {
					kk[j] = -c3[component][j] / (c1[component][j] * kk[j - 1] + c2[component][j]);
					ll[j] = d[component][j] - c1[component][j] * ll[j - 1] / (c1[component][j] * kk[j - 1] + c2[component][j]);
					/*if (!(abs(kk[j]) < 1000 && abs(ll[j]) < 1000)) {
						std::cout << c1[component][j] << " " << c2[component][j] << " " << c3[component][j] << " " << d[component][j];
					}*/
				}
				u_new[component][J - 1] = (*U)[Index{ i, 0, k }][component];
				for (int j = J - 2; j >= 0; --j) {
					Index ind = { i, j, k };
					Index ind_top = { i, j + 1, k };
					u_new[component][j] = kk[j] * u_new[component][j + 1] + ll[j];
				}
			}
			for (int j = 0; j < J; ++j) {
				(*U)[Index{ i, j, k }] = Point5D((*U)[Index{ i, j, k }][0],
					u_new[1][j],
					u_new[2][j],
					u_new[3][j],
					u_new[4][j]);
			}
		}
	}
}