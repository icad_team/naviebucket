#include "tools.h"

std::vector<int> tools::extract_ints(std::ctype_base::mask category, std::string str, std::ctype<char> const& facet)
{
	std::vector<int> result;

	using std::strlen;

	char const *begin = &str.front(),
		*end = &str.back();
	while (1) {
		auto res = facet.scan_is(category, begin, end);

		begin = &res[0];
		end = &res[strlen(res)];
		if (begin == (&str.back() + 1))
			break;
		int number = std::stoi(std::string(begin, end));
		result.push_back(number);
		begin = begin + std::to_string(number).length();
	}

	return result;
}

std::vector<int> tools::extract_ints(std::string str)
{
	return extract_ints(std::ctype_base::digit, str,
		std::use_facet<std::ctype<char>>(std::locale("")));
}


void EigenExtensions::removeRow(Eigen::MatrixXd& matrix, unsigned int rowToRemove)
{
	unsigned int numRows = matrix.rows() - 1;
	unsigned int numCols = matrix.cols();

	if (rowToRemove < numRows)
		matrix.block(rowToRemove, 0, numRows - rowToRemove, numCols) = matrix.block(rowToRemove + 1, 0, numRows - rowToRemove, numCols);

	matrix.conservativeResize(numRows, numCols);
}

void EigenExtensions::removeColumn(Eigen::MatrixXd& matrix, unsigned int colToRemove)
{
	unsigned int numRows = matrix.rows();
	unsigned int numCols = matrix.cols() - 1;

	if (colToRemove < numCols)
		matrix.block(0, colToRemove, numRows, numCols - colToRemove) = matrix.block(0, colToRemove + 1, numRows, numCols - colToRemove);

	matrix.conservativeResize(numRows, numCols);
}

// Calculate (-1)^(i+j) *	M(i,j) - minor of matrix
double EigenExtensions::CalculateMinor(MatrixXd matrix, int i, int j) {
	EigenExtensions::removeRow(matrix, i);
	EigenExtensions::removeColumn(matrix, j);
	return matrix.determinant() * ((i + j) % 2 == 0 ? 1. : -1.);
}

