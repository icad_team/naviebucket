#include <vector>
#include <memory>

using std::vector;

namespace GridsN {
	typedef vector<int> Index;


	template<class T>
	class GridN {
	public:
		GridN(Index direction_sizes) {
			this->_direction_sizes = direction_sizes;
			
			this->_elements = new T[this->GetSize()];
		}

		~GridN() {
			delete[] _elements;
		}

		T& operator[](Index position) {
			//std::cout << position[0] << "\t" << position[1] << "\t" << GetPositionByIndex(position, this->_direction_sizes) << std::endl;
			return *(this->_elements + GetPositionByIndex(position, this->_direction_sizes));
		}

		T& operator[](int i) {
			//std::cout << position[0] << "\t" << position[1] << "\t" << GetPositionByIndex(position, this->_direction_sizes) << std::endl;
			return *(this->_elements + i);
		}

		T operator[](int i) const {
			//std::cout << position[0] << "\t" << position[1] << "\t" << GetPositionByIndex(position, this->_direction_sizes) << std::endl;
			return *(this->_elements + i);
		}

		T operator[](Index position) const {
			return this->_elements[GetPositionByIndex(position, this->_direction_sizes)];
		}

		vector<int> GetDirectionSizes()  const { return _direction_sizes; }

		int GetSize() const {
			int grid_size = 1;
			for (int dimension_number = 0; dimension_number < this->_direction_sizes.size(); ++dimension_number) {
				grid_size *= _direction_sizes[dimension_number];
			}
			return grid_size;
		}

		void operator+=(const GridN<T>& other) {
			for (int i = 0; i < this->GetSize(); ++i) {
				_elements[i] += other._elements[i];
			}
		}

		/*std::shared_ptr<GridN<T> > operator+(const GridN<double>& other) const {
			std::shared_ptr<GridN<T> > result;
			result = std::make_shared<GridN<T> >(GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] + other[i];
			}
			return result;
		}*/

		std::shared_ptr<GridN<T> > operator+(const GridN<T>& other) const {
			std::shared_ptr<GridN<T> > result(new GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] + other[i];
			}
			return result;
		}

		std::shared_ptr<GridN<T> > operator-(const GridN<T>& other) const {
			std::shared_ptr<GridN<T> > result(new GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] - other[i];
			}
			return result;
		}

		void operator-=(const GridN<T>& other) {
			for (int i = 0; i < GetSize(); ++i) {
				_elements[i] -= other._elements[i];
			}
		}

		void operator*=(const GridN<T>& other) {
			for (int i = 0; i < GetSize(); ++i) {
				_elements[i] *= other._elements[i];
			}
		}

		void operator*=(double other) {
			for (int i = 0; i < GetSize(); ++i) {
				_elements[i] *= other;
			}
		}

		std::shared_ptr<GridN<T> > operator*(double other) const {
			std::shared_ptr<GridN<T> > result(new GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] * other;
			}
			return result;
		}

		std::shared_ptr<GridN<T> > operator*(const GridN<double>& other) const {
			std::shared_ptr<GridN<T> > result(new GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] * other[i];
			}
			return result;
		}

		std::shared_ptr<GridN<T> > operator/(const GridN<double>& other) const {
			std::shared_ptr<GridN<T> > result(new GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] / other[i];
			}
			return result;
		}

		std::shared_ptr<GridN<T> > operator/(double other) const {
			std::shared_ptr<GridN<T> > result(new GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] / other;
			}
			return result;
		}

		/*std::shared_ptr<GridN<T> > operator/(const GridN<T>& other_grid) const {
			std::shared_ptr<GridN<T> > result;
			result = std::make_shared<GridN<T> >(GridN<T>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				result->_elements[i] = _elements[i] / other_grid._elements[i];
			}
			return result;
		}*/

		template<class TOut>
		std::shared_ptr<GridN<TOut> > operator,(const GridN<T>& other_grid) const {
			std::shared_ptr<GridN<TOut> > result(new GridN<TOut>(this->GetDirectionSizes()));
			for (int i = 0; i < GetSize(); ++i) {
				(*result)[i] = (_elements[i], other_grid._elements[i]);
			}
			return result;
		}

		/*T GetSum() {
			T result = _elements[0];
			for (int i = 1; i < GetSize(); ++i) {
				result += _elements[i];
			}
			return result;
		}*/

		T GetMax() {
			T result = _elements[0];
			for (int i = 1; i < GetSize(); ++i) {
				if (result < _elements[i]) {
					result = _elements[i];
				}
			}
			return result;
		}

		T GetMin() {
			T result = _elements[0];
			for (int i = 1; i < GetSize(); ++i) {
				if (result > _elements[i]) {
					result = _elements[i];
				}
			}
			return result;
		}
	private:
		T* _elements;
		vector<int> _direction_sizes;

		int GetPositionByIndex(std::vector<int> const index, std::vector<int> const direction_sizes) const {
			if (index.size() != direction_sizes.size()) {
				throw;
			}
			int position = index.back();
			for (int direction = direction_sizes.size() - 2; direction >= 0; --direction) {
				position = index[direction] + direction_sizes[direction] * position;
			}
			return position;
		}
	};

}
