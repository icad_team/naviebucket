#include "DerivativesN.h"

using namespace DerivativesN;
using namespace GridsN;

template <class TPoint>
TPoint& DerivativeFirstN<TPoint>::operator()(Index position, int direction) {
	return this->derivatives_ + this->CalculateGlobalPosition(position, direction);
}

template <class TPoint>
TPoint DerivativeFirstN<TPoint>::CalculateDerivative(Index position, int direction, const GridsN::GridN<TPoint>& points, DerivativeType derivative_type) {
	Index plus_point = position;
	plus_point[direction] += 1;
	Index minus_point = position;
	minus_point[direction] -= 1;

	switch (DerivativeType) {
	case FirstPlus:
		return (points[plus_point] - points[position]) * this->directions_sizes_[direction];
	case FirstMinus:
		return (points[position] - points[minus_point])	* this->directions_sizes_[direction];
	case First:
		if (position[direction] == 0) {
			return CalculateDerivative(position, direction, points, DerivativeType.FirstPlus);
		}
		else if (position[direction] == direction_sizes_[direction] - 1) {
			return CalculateDerivative(position, direction, points, DerivativeType.FirstMinus);
		}
		else {
			return (points[plus_point] - points[minus_point]) * 0.5 * this->directions_sizes_[direction];
		}
	}
}

template <class TPoint>
int DerivativeFirstN<TPoint>::CalculateSize() {
	int size = direction_sizes_.size(); // count of directions
	// number of points
	for (int dimension_number = 0; dimension_number < direction_sizes_.size(); ++dimension_number) {
		size *= direction_sizes_[dimension_number];
	}
	return size;
}

template <class TPoint>
int DerivativeFirstN<TPoint>::CalculateGlobalPosition(Index position, int direction) {
	if (position.size() != this->direction_sizes_) {
		throw;
	}
	int global_position = direction;
	for (int dimension = position.size() - 1; dimension >= 0; --dimension) {
		global_position = position[dimension] + this->direction_sizes_[dimension] * position;
	}
	return global_position;
}


template <class TPoint>
DerivativeFirstN<TPoint>::DerivativeFirstN<TPoint>(vector<int> direction_sizes) {
	this->direction_sizes_ = direction_sizes;

	int size = this->CalculateSize();
	this->derivatives_ = new TPoint[size];
}


template <class TPoint>
DerivativeFirstN<TPoint>::DerivativeFirstN<TPoint>(const GridsN::GridN<TPoint>& points, DerivativeType derivative_type) {
	this->direction_sizes_ = points.GetDirectionSizes();

	int size = this->CalculateSize();
	this->derivatives_ = new TPoint[size];

	int count_points = size / direction_sizes.size();

	// Calculate derivatives in each direction
	for (int global_index = 0; global_index < size; ++global_index) {
		int global_position = global_index;
		Index position;
		for (int dimension_number = 0; dimension_number < direction_sizes.size(); ++dimension_number) {
			index.push_back(global_position % direction_sizes[dimension_number]);
			global_position /= direction_sizes[dimension_number];
		}
		int direction = global_position;
		this->derivatives_[global_index] = CalculateDerivative(position, direction, TPoint* points, derivative_type);
	}
}

template <class TPoint>
DerivativeFirstN<TPoint>::~DerivativeFirstN<TPoint>() {
	delete[] derivatives_;
}

