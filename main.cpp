//// source 1: "Dinamika prostranstvennyh vihrevyh techenii v neodnorodnoy atmosfere" (Belocerkovsky, Andrushenko, Shevelev)
#include "Navie.h"

using namespace PointsN;
using namespace Navie;
using std::cout;
using std::endl;

int main() {
	const string grid_file = "grid.dat";
	const string parameters_file = "parameters.dat";

	const string result_file = "result.dat";
	const int ITERATIONS_NUMBER = 1000;

	// Init data
	Mesh mesh(grid_file);
	// Read parameters
	std::ifstream parameters_stream(parameters_file);
	mesh.ReadParameters(parameters_stream);
	parameters_stream.close();

	mesh.InitExternalProperties();
	
	for (int iteration = 0; iteration < ITERATIONS_NUMBER; ++iteration){
		cout << "ITERATION NUMBER #" << iteration + 1 << endl;
		mesh.SetSymmetry();
		mesh.SetInternalBoundary();


		// calcucalte preasure by energy
		//cout << "	Max delta preasure: " << mesh.CalculatePreasure() << endl; 

		mesh.GetStatistics();
		mesh.CalculateTimeStep();
		double alpha = 0.25;
		//PREDICTOR
		mesh.CalculateViscous();
		SharedGridPoint5D U_corrector = mesh.ExtractU();
		SharedGridPoint5D U_predictor = mesh.ExtractU();

		mesh.CalculateStep(U_predictor, FirstMinus);
		mesh.Smooth(alpha, U_predictor, U_corrector);
		mesh.ExtractMacroparameters(U_predictor);
		//mesh.CalculatePreasure();
		//CORRECTOR
		mesh.CalculateViscous();
		mesh.CalculateStep(U_corrector, FirstPlus);

		SharedGridPoint5D U = *(*U_predictor + *U_corrector) * 0.5;
		mesh.Smooth(alpha, U, U_predictor);
		//mesh.Sweep(U);
		mesh.ExtractMacroparameters(U);
	}
	mesh.SetSymmetry();
	mesh.SetInternalBoundary();
	//mesh.CalculatePreasure();
	mesh.WriteResult(result_file);

	//string a;
	//std::cin >> a;
	return 0;
}


//int main() {
//	// TEST
//	vector<int> sizes = { 5, 5 };
//	GridN<Point3D> grid_test(sizes);
//	for (int i = 0; i < 5; ++i) {
//		for (int j = 0; j < 5; ++j) {
//			vector<int> ind = { i, j };
//			double x = i / 4.;
//			double y = j / 4.;
//			double z = x + y;
//			Point3D point(x, y, z);
//			grid_test[ind] = point;
//		}
//	}
//
//	DerivativeFirstN<Point3D> deriv(grid_test, First);
//
//
//	std::cout << "TEST POINTS DER" << std::endl;
//	vector<int> test_pt = { 1, 4 };
//	auto res0 = deriv(test_pt, 0);
//	std::cout << res0[0] << "\t" << res0[1] << "\t" << res0[2] << std::endl;
//	vector<int> test_pt1 = { 2, 3 };
//	auto res1 = deriv(test_pt1, 1);
//	std::cout << res1[0] << "\t" << res1[1] << "\t" << res1[2] << std::endl;
//
//	auto derivative_direction = CalculateDerivativeDirection(grid_test, 1, First);
//	std::cout << "Derivative direction" << std::endl;
//	std::cout << (*derivative_direction)[test_pt][0] << "\t" << (*derivative_direction)[test_pt][1] << "\t" << (*derivative_direction)[test_pt][2] << std::endl;
//	std::cout << "Derivative direction" << std::endl;
//	auto derivative_direction1 = CalculateDerivativeDirection(grid_test, 1, FirstMinus);
//	std::cout << (*derivative_direction1)[test_pt][0] << "\t" << (*derivative_direction1)[test_pt][1] << "\t" << (*derivative_direction1)[test_pt][2] << std::endl;
//	
//	auto vector_multiplication = grid_test.operator,<double>(grid_test);
//	auto vector_sum = grid_test + grid_test;
//	auto vector_dif = grid_test - grid_test;
//	auto vector_mult_double = grid_test * 10.;
//	auto vector_div_double = grid_test / 10.;
//	
//	std::ofstream out_file("test.dat");
//	out_file << "VARIABLES = \"X\", \"Y\", \"Z\"" << std::endl;
//	out_file << "ZONE F=POINT, I=" << 5 << ", J=" << 5 << std::endl;
//
//	for (int i = 0; i < 5; ++i) {
//		for (int j = 0; j < 5; ++j) {
//				Index current_index = { i, j };
//				Point3D& current_point = grid_test[current_index];
//				Point3D& current_point_der_dir = (*derivative_direction)[current_index];
//
//				out_file << "Point\t" 
//					<< current_point[0] << "\t"
//					<< current_point[1] << "\t"
//					<< current_point[2] << "\t" << std::endl
//
//
//					<< "current_point_der_dir\t"
//					<< current_point_der_dir[0] << "\t"
//					<< current_point_der_dir[1] << "\t"
//					<< current_point_der_dir[2] << "\t" << std::endl
//					<< "vector_multiplication\t"
//					<< (*vector_multiplication)[current_index]<< "\t" << std::endl
//					<< "vector_sum\t"
//					<< (*vector_sum)[current_index][0] << "\t"
//					<< (*vector_sum)[current_index][1] << "\t"
//					<< (*vector_sum)[current_index][2] << "\t" << std::endl
//					<< "vector_dif\t"
//					<< (*vector_dif)[current_index][0] << "\t"
//					<< (*vector_dif)[current_index][1] << "\t"
//					<< (*vector_dif)[current_index][2] << "\t" << std::endl
//					<< "vector_mult_double\t"
//					<< (*vector_mult_double)[current_index][0] << "\t"
//					<< (*vector_mult_double)[current_index][1] << "\t"
//					<< (*vector_mult_double)[current_index][2] << "\t" << std::endl
//					<< std::endl;
//		}
//	}
//	out_file.close();
//	return 0;
//}
