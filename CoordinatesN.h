#ifndef COORDINATESN_H
#define COORDINATESN_H

#include <array>            // std::array
#include <vector>            // std::array

#define STATIC_ASSERT( e ) static_assert( e, "!(" #e ")" )

namespace PointsN {


	template<typename T, int nDimensions = 2>
	class PointN {
	protected:
		std::array<T, nDimensions> elements_;
	public:
		T const& operator[](int const i) const {
			return elements_[i];
		}

		void operator+=(PointN<T, nDimensions> const& other) {
			for (int i = 0; i < nDimensions; ++i) {
				elements_[i] += other.elements_[i];
			}
		}

		void operator-=(PointN<T, nDimensions> const& other) {
			for (int i = 0; i < nDimensions; ++i) {
				elements_[i] -= other.elements_[i];
			}
		}

		PointN<T, nDimensions>& operator=(PointN<T, nDimensions> const& other) {
			this->elements_ = other.elements_;
			return *this;
		}

		PointN<T, nDimensions> operator+(PointN<T, nDimensions> const& a) const  {
			PointN<T, nDimensions> result = *this;
			result += a;
			return result;
		} 

		PointN<T, nDimensions> operator-(PointN<T, nDimensions> const& a) const  {
			PointN<T, nDimensions> result = *this;
			result -= a;
			return result;
		}

		void operator*=(double b) {
			for (int i = 0; i < nDimensions; ++i) {
				elements_[i] *= b;
			}
		}

		T Modul() const {
			return this->operator,(*this);
		}

		void operator/=(double b) {
			for (int i = 0; i < nDimensions; ++i) {
				elements_[i] /= b;
			}
		}

		PointN<T, nDimensions> operator*(double b) const {
			PointN<T, nDimensions> result = *this;
			result *= b;
			return result;
		}

		PointN<T, nDimensions> operator/(double b) const {
			PointN<T, nDimensions> result = *this;
			result /= b;
			return result;
		}

		T operator,(PointN<T, nDimensions> const& point_r) {
			T result = this->elements_[0] * point_r.elements_[0];
			for (int i = 1; i < nDimensions; ++i) {
				result += this->elements_[i] * point_r.elements_[i];
			}
			return result;
		}

		PointN<T, nDimensions> CalculateSymmetry(int direction) {
			PointN<T, nDimensions> result = *this;
			result.elements_[direction] = -result[direction];
			return result;
		}

		PointN<T, nDimensions> Abs() {
			PointN<T, nDimensions> result = *this;
			result.elements_[direction] = abs(result[direction]);
			return result;
		}

		//VectorN<T, nDimensions> ToVector();
		PointN() {}

		PointN(std::ifstream &in_stream) {
			for (int i = 0; i < nDimensions; ++i) {
				in_stream >> elements_[i];
			}
		}

		PointN(T x, T y)
		{
			STATIC_ASSERT(nDimensions == 2);
			elements_[0] = x;
			elements_[1] = y;
		}

		PointN(T x, T y, T z)
		{
			STATIC_ASSERT(nDimensions == 3);
			elements_[0] = x;
			elements_[1] = y;
			elements_[2] = z;
		}

		PointN(T x1, T x2, T x3, T x4, T x5)
		{
			STATIC_ASSERT(nDimensions == 5);
			elements_[0] = x1;
			elements_[1] = x2;
			elements_[2] = x3;
			elements_[3] = x4;
			elements_[4] = x5;
		}
	};

	//template<typename T, int nDimensions = 2>
	//class VectorN : public PointsN::template PointN<typename T, nDimensions> {
	//public:
	//	T operator,(VectorN<T, nDimensions> const& point_r) {
	//		T result = this->elements_[0] * point_r.elements_[0];
	//		for (int i = 1; i < nDimensions; ++i) {
	//			result += this->elements_[i] * point_r.elements_[i];
	//		}
	//		return result;
	//	}

	//	VectorN<T, nDimensions> CalculateSymmetry(int direction) {
	//		VectorN<T, nDimensions> result = *this;
	//		result.elements_[direction] = -result[direction];
	//		return result;
	//	}

	//	VectorN(PointN<T, nDimensions> point) : PointN<T, nDimensions>(point) {};

	//	VectorN() {}

	//	VectorN(T x, T y) {
	//		STATIC_ASSERT(nDimensions == 2);
	//		elements_[0] = x;
	//		elements_[1] = y;
	//	}

	//	VectorN(T x, T y, T z) {
	//		STATIC_ASSERT(nDimensions == 3);
	//		elements_[0] = x;
	//		elements_[1] = y;
	//		elements_[2] = z;
	//	}
	//};
}
#endif //COORDINATESN_H