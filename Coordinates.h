#ifndef COORDINATES_H
#define COORDINATES_H
#include <math.h>
#include <fstream>
#include <string>
#include <new>
#include <iostream>
#include "tools.h"

using std::string;
using std::endl;


const double pi = 4 * atan(1.);

namespace Coordinates {
	class Point {
	public:
		Point();
		Point(double in_x, double in_y, double in_z);
		Point::Point(std::ifstream &in_stream);
		Point operator+(Point point_r);
		Point operator-(Point point_r);
		Point operator-();
		Point operator=(Point point_r);
		Point operator*(double number_r);
		//void SetPoint(double in_x, double in_y, double in_z);
		double GetX();
		double GetY();
		double GetZ();
		void WriteInFile(std::ofstream& out_file);
		double norm();
	protected:
		double x;
		double y;
		double z;
	};

	class Vector : public Point {
	public:
		Vector();
		Vector(Point point_from, Point point_to);
		Vector(double in_x, double in_y, double in_z);
		double norm();
		double operator,(Vector point_r);
		double operator,(Point point_r);
		Vector operator*(Vector point_r);
		Vector operator*(double number_r);
		Vector operator+(Vector vector_r);
		Vector operator-(Vector vector_r);
		Point ToPoint();
	};

	struct Derivatives {
		// d(x)/d(ksi) d(y)/d(ksi) d(z)/d(ksi) 
		Vector ksi_inv;
		// d(x)/d(eta) d(y)/d(eta) d(z)/d(eta) 
		Vector eta_inv;
		// d(x)/d(dzt) d(y)/d(dzt) d(z)/d(dzt) 
		Vector dzt_inv;

		// d(ksi)/d(x)/J d(ksi)/d(y)/J d(ksi)/d(z)/J
		Vector ksi;
		// d(eta)/d(x)/J d(eta)/d(y)/J d(eta)/d(z)/J 
		Vector eta;
		// d(dzt)/d(x)/J d(dzt)/d(y)/J d(dzt)/d(z)/J
		Vector dzt;
		double jacobian_det;
	};
}

#endif /* COORDINATES_H */