#include "CoordinatesN.h"
#include "DerivativesN.h"
#include <math.h>
#include <fstream>
#include <string>
#include <new>
#include <iostream>
#include "tools.h"

using namespace PointsN;
using namespace DerivativesN;
using namespace GridsN;
using std::string;
typedef PointN<double, 5> Point5D;
typedef PointN<double, 3> Point3D;

typedef std::shared_ptr<GridN<Point3D> > SharedGridPoint3D;
typedef std::shared_ptr<GridN<Point5D> > SharedGridPoint5D;
typedef std::shared_ptr<GridN<double> > SharedGridDouble;

typedef std::shared_ptr<DerivativeFirstN<Point3D> > SharedGridDerivFirstPoint3D;


namespace Navie {
	class Mesh {
	public:
		Mesh(string file_name);
		~Mesh();
		void ReadParameters(std::ifstream& parameters_stream);
		void InitExternalProperties(double external_preasure = 1., double external_density = 1.);
		
		void SetSymmetry();
		void SetInternalBoundary();
		// returns max delta preasure
		//double CalculatePreasure();
		void GetStatistics();
		double CalculateTimeStep();
		// "PROGONKA"
		void Sweep(SharedGridPoint5D& U);

		void CalculateViscous();
		void Smooth(double alpha, SharedGridPoint5D& U, const SharedGridPoint5D& U_glad);
		void CalculateStep(SharedGridPoint5D& U, DerivativeType derivative_type);
		void ExtractMacroparameters(const SharedGridPoint5D& U);
		SharedGridPoint5D ExtractU();

		void WriteResult(string file_name);
	private:
		const double MAX_MIN_TIME_STEP = 1000;

		int I, J, K;
		// Steps i, j, k
		vector<double> h_step;
		double MACH;
		// 1./Reynolds
		double RE_inv;
		double PRANDL;
		double ATTACK_ANGLE;
		double CURRANT;
		// Heat capacity ratio https://en.wikipedia.org/wiki/Heat_capacity_ratio
		double GAMMA;
		double TEMPERATURE_BOUNDARY;

		SharedGridPoint3D _coordinates;
		SharedGridDerivFirstPoint3D _first_derivatives_inv;
		SharedGridDerivFirstPoint3D _first_derivatives;
		SharedGridDouble _jacobian;

		// Macroparameters
		SharedGridPoint3D _velocity;
		SharedGridDouble _preasure;
		SharedGridDouble _density;
		//SharedGridDouble _temperature;
		SharedGridDouble _energy;

		double static CalculateTotalInternalEnergy(double gamma, double preasure, double density, Point3D velocity);
		double static CalculatePreasureByParameters(double gamma, double energy, double density, Point3D velocity);
		double static CalculateTimeStepDirectionByParameters(double gamma, double currant, Point3D derivative_direction, Point3D velocity, double preasure, double density, double h_step);

		//SharedGridPoint5D _U;
		SharedGridPoint5D _E;
		SharedGridPoint5D _F;
		SharedGridPoint5D _G;
		SharedGridPoint5D _S;

		//SharedGridPoint5D _S_old;

		double _min_timestep;
		SharedGridDouble _timestep;
	};
}
